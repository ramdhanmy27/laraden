#Dokumentasi

#1. Application Definition
Aplikasi yang dibuat perlu didefinisikan agar bisa terdaftar pada system.

##1.1. Struktur Definition

```php
return [
    "name" => "Human Resource", // nama aplikasi
    "db-name" => "hr", // database aplikasi
    "description" => "Aplikasi Human Resource", #optional

    // app dependencies
    "require" => [
        "file-management" => 2,
    ],

    // features
    "features" => [

        // key feature
        "pegawai" => [

            "name" => "Data Pegawai", // nama fitur
            "description" => "Informasi Data Pegawai", #optional

            // credentials
            "credentials" => [
                "list-view",
                "print",
                "keluarga",
                "pendidikan terakhir",
                "kontak",
            ],
        ]
    ]
];
```

##1.2. Compile Definitions

```bash
php artisan compile-me
```


#2. Routing
##2.1. Feature

Untuk meregistrasikan feature pada aplikasi, gunakan seperti dibawah ini.

```php
Route::feature("key-fitur", function() {
    Route::get("url", "NamaController@index");
    // ...
})
```

##2.2. Menu
Untuk membuat menu navigasi pada halaman web, anda hanya cukup menambahkannya saja pada routing aplikasi. Aplikasi yang akan me-render data menu tersebut ke dalam bentuk html.

###2.2.1. Menu Router
####a. Basic

Sederhananya, menu dapat didefinisikan seperti di bawah.

```php
Route::menu("Judul Menu");

Route::menu([
    "title" => "Judul Menu", 
    "url" => "url/app", 
    "icon" => "fa fa-plus",
]);
```

####b. Nested Menu

Routing menu juga bisa digunakan seperti fungsi `Route::group`. Anda bisa definisikan Middleware, Namespaces, Sub-Domain Routing dan Route Prefixes bersamaan dengan menu.

```php
Route::menu([
    "title" => "Judul Menu", 
    "url" => "url/app", 
    "icon" => "fa fa-plus",
    "middleware" => "auth",
    "prefix" => "nested-menu",
], function() {
    Route::menu("Sub level", function() {
        Route::menu([
            "title" => "Menu", 
            "url" => "url/app/2", 
            "icon" => "fa fa-user",
        ]);
    });
    
    Route::get("/", "AplikasiController@index");
});

/** Hasil

Judul Menu (url/app)
  |-- Sub level (#)
        |-- Menu (url/app/2)
*/
```

###2.2.2. Menu Action

```php
Route::get('edit/{id}/{hashid}', 'FormController@form')
    ->menu("Judul", "css-icon", [$id, md5($id)]);
```

###2.2.3. Menu Controller

```php
Route::controller("contoh", "ContohController")
        ->menu("Home", "glyphicon glyphicon-home")
        ->menu("Settings", "fa fa-cog", "url-transaction");
```


###2.2.4. Menambahkan Kondisi Pada Menu
####a. Menu Router

```php
Route::menu("Judul Menu")->on(function() {
    return Auth::check(); // boolean
});
```

####b. Menu Action

```php
Route::get('edit/{id}/{hashid}', 'FormController@form')
    ->menu("Judul", "css-icon", [$id, md5($id)])
    ->on(function() {
        return true;
    });
```

####c. Menu Controller

```php
Route::controller("contoh", "ContohController")
    ->menu("Home")->on(function() {
        return false;
    })
    ->menu("Contoh Transaction")->on(function() {
        return Auth::check();
    });
```

#3. Akses Management
Jenis pengecekan hak akses pada framework hanya tersedia salah satu, yaitu Credentials atau Permissions. 

##3.1. Credentials
Credential yang dicek oleh system adalah credential bedasarkan feature yang sedang diakses.

###a. And Operator

```php
// user memiliki kedua credential "list-view" dan "kontak"
if (Auth::user()->hasAbility(["list-view", "kontak"]))
{
    // ...
}
```

###b. Or Operator

```php
// user memiliki salah satu credential dari "list-view" atau "kontak"
if (Auth::user()->hasOneAbility(["list-view", "kontak"]))
{
    // ...
}
```

##3.2. Permissions
###3.2.1. Registrasi
Berikut contoh untuk meregistrasikan permissions pada sebuah controller

```php
class ContohController extends Controller {
    public static $permissions = [
        ["create", "Tambah Data"],
        ["update"],
        "delete",
    ];
}
```

Anda dapat memilih metode manapun yang anda suka. Pada index pertama isikan nama permision lalu isikan juga deskripsi pada index selanjutnya. Atau cukup isikan nama permisionnya saja dan controller yang akan mengurus sisanya.

###3.2.2. Penggunaan

Permission yang sudah diregistrasikan akan tesimpan dalam database dengan format `contoh:create`. Jika anda ingin mengecek apakah user memiliki hak akses `create` pada `ContohController`. Anda bisa tuliskan sebagai berikut :

```php
if (Auth::user()->can('contoh:create')) {
    // eksekusi create
}
```

Namun apabila itu terlalu panjang untuk anda, bisa juga bisa dituliskan seperti ini:

```php
// nama permissions mengikuti controller yang sedang diakses
if (can('create')) {
    // eksekusi create
}
```

#4. Tool & Helpers
##4.1. Form Builder
>https://laravelcollective.com/docs/5.2/html

###4.1.1. Text

```php
{!! Form::text('name', 'Maxlength', ["maxlength" => 10]) !!}
```

###4.1.2. Date

```php
{!! Form::date('name', date("d/m/Y")) !!}
```

###4.1.3. Date Range

```php
$to = $from = date("d/m/Y");

{!! Form::dateRange('name', [$from, $to]) !!}
```

###4.1.4. Time

```php
{!! Form::time('name', 'Time') !!}
```

###4.1.5. Checkboxes

```php
{!! Form::checkboxes('name[]', ["v1" => "label 1", "v2" => "label 2"], ["v1"]) !!}
```

###4.1.6. Radios

```php
{!! Form::radios('name[]', ["v1" => "label 1", "v2" => "label 2"], ["v1"]) !!}
```

###4.1.7. Toggle Switch

```php
{!! Form::toggleSwitch('name', "value", true, [], "switch-sm switch-dark") !!}
```

###4.1.8. Form Group
Gunakan Form Group jika anda ingin membuat form input beserta dengan labelnya. Seperti HTML di bawah ini.

```html
 <div class="form-group">
     <label for="name" class="col-md-3 control-label">Label</label>
     <div class="col-md-6">
         <input class="form-control valid" name="name" value="value" id="name" type="text">
     </div>
 </div>
```

Berikut cara penggunaannya :

```php
{!! Form::group('date-range', 'name', 'Date Range') !!}
{!! Form::group('text', 'name', 'Maxlength', "value", ["maxlength" => 20]) !!}
```

##4.2. Table Grid
###4.2.1. Membuat DataGrid

```php
use Grid;

/**
 * Initialize DataGrid instance
 * 
 * @param  Illuminate\Database\Query\Builder  $query
 * @param  array  $field   
 * @param  integer $paginate
 * @return DataGrid
 */
$grid = Grid::init(DB::table("nama-table"), ["field1", "field2"], 100);

/**
 * Initialize DataGrid instance with model instance
 * 
 * @param  String|Class  $model
 * @param  array  $field   
 * @param  integer $paginate
 * @return DataGrid
 */
$grid = Grid::model(MyModel::class, ["field1", "field2"], 100);

// render DataGrid
echo $grid;
```

###4.2.2. Action Buttons
####Menampilkan Action Buttons

```php
$grid->add("field", "Action Header")->cell(function($val, $model) {
    return Grid::action($model, $base_url);
});

// render DataGrid
echo $grid;
```

####Custom Action Buttons

```php
$grid->add("field", "Action Header")->cell(function($val, $model) {
    return Grid::action($model, $base_url, [
        "view" => function($id, $model) {
            return "custom view button";
        },
        "my-custom" => function($id, $model) {
            return Html::link("url/$id", Html::icon("fa fa-home"));
        }
    ], "my-custom|view|edit");
});

// render DataGrid
echo $grid;
```

###4.2.3. Referensi
> https://github.com/zofe/rapyd-laravel/wiki/DataGrid

##4.3. Pesan Pemberitahuan
###4.3.1. Flash Messages

```php
use Flash;

Flash::push("Pesan Info", "info");
```

```php
use Html;

Flash::info("Information");
Flash::warning("Pesan Warning");
Flash::danger(["Pesan Error", "Pesan Error #2"]);
Flash::success(Html::icon("fa fa-check")." Pesan Success");
```

###4.3.2. Notification Alert
Hampir sama seperti Flash, hanya method danger diganti dengan error.

```php
use Notif;

Notif::push("you've got a Notification!", "error");
```

```php
Notif::error("Failed");
Notif::success("Success");
Notif::info([
    [
        "msg" => "you've got a Notification!", 
        "title" => "Information", 
        // "icon" => "fa fa-info", 
        // "class" => "bg-info",
        // "type" => "info",
    ]
]);
Notif::warning([
    [
        "msg" => "you've got a Warning!", 
        "title" => "Warning", 
        "icon" => "fa fa-exclamation-triangle", 
    ],
    [
        "msg" => "you've got a Warning!", 
        "title" => "Warning #2", 
        "icon" => "fa fa-home", 
    ]
]);
```

##4.4. Traits
###4.4.1. CRUD

```php
namespace App\Http\Controllers;

class AppController extends Controller
{
    use Traits\CrudController;

    // Tidak perlu ditulis jika tidak perlu
    public function __construct() {
        $this->crudInit(function($config) {
            // secara default akan mengikuti nama controller
            // default: App\Models\App
            $config->model = MyModel::class;
            
            // Judul Halaman
            $config->title = "Judul";
            
            // base url controller
            $config->base_url = "admin";
            
            // definisi form add/edit
            // secara default akan mengikuti field pada table
            $config->form = [
                ["text", "name"],
                ["text", "display_name"],
                ["textarea", "description"],
            ];
            
            // kolom table list
            $config->table_field = ["name", "email", "created_at", "updated_at"];
            
            // kolom view list
            $config->view_field = ["id" => "ID", "name", "email", "created_at", "updated_at"];
        });
    }
}
```

####CRUD Action
Controller yang menggunakan Trait ini akan disediakan beberapa method action beserta viewnya.

__getIndex()__
view: layout.crud.index

__getAdd()__
view: layout.crud.add, layout.crud.form

__getEdit(\$id)__
view: layout.crud.edit, layout.crud.form

__getView(\$id)__
view: layout.crud.view

__postAdd(Request \$req)__
redirect to base_url

__postEdit(Request \$req, \$id)__
redirect to base_url

__postDelete(Request \$req, \$id)__
redirect back

Jika anda ingin menghiraukan method action yang sudah disediakan, anda bisa menuliskan kembali pada Controller yang dibuat. Begitu pula dengan view, jika anda membuat view dengan nama yang sama (contoh: `/resources/views/nama-controller/index.blade.php`) maka aplikasi akan menggunakan view yang anda buat.

##4.5. Lainnya
###4.5.1. String Formatter
####xml_encode

Contoh Penggunaan:

```php
echo htmlentities(xml_encode(["data" => ["item" => [1, 2, 3]]]));

/* Hasil :
<data> 
  <item>1</item> 
  <item>2</item> 
  <item>3</item> 
</data>
*/
```

####currency
```php
currency(10000); // Rp 10.000
currency(10000, 2, "$"); // $ 10.000,00
```

###4.5.2. Debug
Karena framework ini merupakan turunan dari framework laravel, tentunya sudah disediakan fungsi `dd` untuk keperluan debugging. Akan tetapi apabila masih terasa kurang, sudah ditambahkan pula beberapa fungsi yang mendukung untuk mendebug program anda.

####prins 
Menampilkan isi variable dari `$var_1` sampai `$var_n`

```php
prins($var_1, $var_2, ..., $var_n);
```

####prin
Menampilkan isi variable dari `$var_1` sampai `$var_n` dan `exit`

```php
prin($var_1, $var_2, ..., $var_n);
```

####traces
Backtrace process dan menampilkan isi variable dari `$var_1` sampai `$var_n`

```php
traces($var_1, $var_2, ..., $var_n);
```

####trace
Backtrace process dan menampilkan isi variable dari `$var_1` sampai `$var_n` dan `exit`

```php
trace($var_1, $var_2, ..., $var_n);
```

###4.5.3. Case Converter
####camel2id

```php
echo camel2id("CamelCase"); // camel-case
echo camel2id("CamelCase", "_"); // camel_case
```

####id2title

```php
echo id2title("object-id"); // Object Id
echo id2title("object_id", "_"); // Object Id
```

####id2camel

```php
echo id2camel("object-id"); // ObjectId
echo id2camel("object_id", "_"); // ObjectId
```

###4.5.4. View
####View
Penggunaan view pada biasanya disertai dengan lokasi file view dan data yang  ingin dimasukkan kedalam view dalam bentuk array.

```php
view("path.to.view", ["var1" => $data1]);
```

Tapi anda bisa menyingkatnya jika anda menggunakan Controller, seperti ini.


```php
class ViewController extends Controller {
    
    public function getIndex() {
        // sama dengan view("view.index", []);
        return view([]);
    }
    
    public function report() {
        // sama dengan view("view.report", ["data" => [1, 2, 3]]);
        return view(["data" => [1, 2, 3]]);
    }
}
```

####View Theme

Pada framework ini sudah disediakan dua tema secara default (`front` dan `backend`). Untuk menggunakan salah satunya anda bisa tambahkan nama tema sebagai namespace pada path view seperti ini.

```php
// path: resources/theme/backend/views/home/index.blade.php
view("backend::home.index");
```

Atau jika ingin menggunakan layout luarnya saja bisa digunakan seperti ini.

```php
extends("backend::app")

@section("content")
    <div>Bla bla bla...</div>
    
    // path: resources/theme/backend/views/ui/modal.blade.php
    @include("backend::ui.modal")
@endsection
```

>__Note :__ Jika view aplikasi modular yang dieksekusi memiliki path yang sama secara global, gunakan namespace `app` (contoh: `app::view.path`) agar menggunakan view yang ada pada module.

#####Assets

File assets javascript dan css juga ada yang terpisah untuk setiap tema. Untuk mendapatkan path file asset dari tema yang diinginkan, penggunaannya hampir sama seperti pada view.

```php
// path: public/js/app.js
asset("js/app.js")

// path: public/theme/front/js/app.js
asset("front::js/app.js")

// path: public/theme/backend/js/app.js
asset("backend::css/app.css")
```

####view_exists

```php
view_exists("path.to.view"); // boolean
```

####first_view
Hampir sama seperti fungsi view, hanya saja fungsi ini hanya merender salah satu dari view path yang diberikan. View yang ditampilkan adalah yang pertama ditemukan. Jika file view hanya tersedia "path.to.view2", maka view yang di render adalah "path.to.view2".

```php
first_view(["path.to.view", "path.to.view2"], ["data" => $data]);
```

###4.5.5. Array Helper
####is_assoc

```php
is_assoc([2, 1]); // false
is_assoc([1 => 2, 1]); // true
is_assoc(['a' => 1, 'b' => '2']); // true
```

#5. Javascript
##5.1. Object fn
###5.1.1. csrf
berisi nilai token csrf

```javascript
console.log(fn.csrf);
```

###5.1.2. event
Dalam object ini berisi semua kebutuhan untuk mengurusi keperluan event javascript, khususnya JQuery. Berikut beberapa fungsi yang ada pada object ini.

####init
Gunakan fungsi ini untuk menginisialisasi event

```javascript
fn.event.init($("#your-id"));
```

####push & trigger
Jika ingin melakukan sesuatu pada saat fungsi `fn.event.init` dipanggil, gunakan push. Fungsi yang sudah di-push bisa di trigger dengan fungsi `fn.event.trigger()`. Biasanya fungsi ini digunakan untuk melakukan inisialisasi event saat memuat HTML dengan AJAX.

```javascript
fn.event.push(function() {
    $("#element").click(function(){
        alert('wow');
    })
});
```

###5.1.3. url
Jika anda terlalu malas untuk menuliskan url yang panjang dengan http query yang banyak anda bisa menggunakan fungsi ini.

```javascript
fn.url("pegawai", {q: "budi", sort:"asc"})
// hasil: http://domain.com/pegawai?q=budi&sort=asc
```

###5.1.4. format

####num
Memformat angka menjadi format uang (seperti: 10.000).

```javascript
var money = fn.format.num("10000")
// money -> 10.000
```

###5.1.5. grep
Fungsi ini sama seperti fungsi grep pada JQuery. Anda bisa memfilter data object atau array sesuai dengan kriteria yang anda inginkan. Namun karena fungsi ini dibuat dengan fungsi native javascript, anda bisa menggunakannya tanpa harus menambahkan JQuery.

```javascript
var data = {a: 1, b: 2};
var grep = fn.grep(data, function(val, i) {
    return val==1 && i=='a';
})
// grep -> [1]
```

###5.1.6. exists
Hampir sama seperti grep, fungsi ini mengolah data object atau array sebagai inputannya. Namun exists ini bertujuan untuk mencari tahu apakah terdapat data yang masuk kedalam kriteria yang anda tuliskan.

```javascript
var data = [1, 2];
var exists = fn.exists(data, function(val, i) {
    return val==2 && i==1;
})
// exists -> true
```

###5.1.7. isNumeric
Memeriksa apakah nilai variable bertuliskan angka.

```javascript
fn.isNumeric(2); // true
fn.isNumeric('2'); // true

fn.isNumeric('a'); // false
fn.isNumeric('2a'); // false
```

###5.1.8. int
Memformat nilai non-integer menjadi integer.

```javascript
fn.int(2); // 2
fn.int(2.3); // 2
fn.int('2.4'); // 2

fn.int('2,4'); // 0
fn.int('2x'); // 0
fn.int(NaN); // 0
fn.int(undefined); // 0
fn.int(null); // 0
fn.int([]); // 0
fn.int({}); // 0
```

###5.1.9. alertError
Menampilkan modal error.

```javascript
fn.alertError("Messages", "Title");
```

###5.1.10. alert
Menampilkan alert modal.

```javascript
fn.alert("Messages", "Title");

fn.alert(["Messages1", "Messages2"], "Title");
// <li>Messages1</li> <li>Messages2</li>

fn.alert({a: "Messages1", b: "Messages2"}, "Title");
// <li>Messages1</li> <li>Messages2</li>
```

###5.1.11. confirm
Menampilkan modal konfirmasi

```javascript
fn.confirm({
    // secara [default: #modal-confirm]
    selector: "#modal-confirm",
    
    // Judul Modal [default: "Warning!"]
    title: "Title",
    
    // Content Modal [default: "Are you Sure ?"]
    body: "Messages",
    
    // dieksekusi saat user menekan tombol setuju
    yes: function() {
        alert("Confirmed");
    },
    // dieksekusi saat user menekan tombol batal
    no: function() {
        alert("Canceled");
    },
});
```

###5.1.12. empty
Memeriksa apakah variable kosong atau tidak.

```javascript
fn.empty(undefined) // true
fn.empty(null) // true
fn.empty(" ") // true
fn.empty({}) // true
fn.empty([]) // true

fn.empty(0) // false
fn.empty("string") // false
fn.empty([1,2,3]) // false
fn.empty({a: 1, b: 2}) // false
```

###5.1.13. isset
Object yang memiliki banyak level anak kadang merepotkan untuk diperiksa. Dengan fungsi ini anda dapat melakukannya seperti berikut :

```javascript
var obj = {
    a : {
        aa: 1, 
        ab: { 
            aba: 1 
        }
    }
}

fn.isset(obj.a.aa) // true
fn.isset(obj.a, "ab", "aba") // true
fn.isset(0) // true
fn.isset("") // true
fn.isset([]) // true
fn.isset({}) // true

fn.isset(obj.a.a, "abc") // false
fn.isset(obj, "a", "ab", "abc") // false
fn.isset(obj2) // false
fn.isset(undefined) // false
fn.isset(null) // false
```

###5.1.14. flash

Menampilkan flash message

```javascript
// success | danger | info | warning
fn.flash("Messages", "info");
fn.flash(["Messages #1", "Messages #2"], "danger");
```

###5.1.15. notif

Menampilkan Pesan Notifikasi

```javascript
// success | danger | info | warning/notice
fn.notif("Notification Messages", "info");
fn.notif(["Notification #1", "Notification #2"], "warning");

// dark | primary
fn.notif({
    title: "Nightmare :3",
    msg: "Dark Messages",
    icon: "fa fa-user",
    class: "notification-dark",
});

fn.notif([
    {
        title: "Primary #1",
        msg: "Messages",
        icon: "fa fa-home",
        class: "notification-primary",
    },
    {
        title: "Primary #1",
        msg: "Messages",
        icon: "fa fa-home",
        class: "notification-primary",
    },
]);
```

###5.1.16. isArray
Memeriksa apakah nilai variable bertipe array.

```javascript
fn.isArray([1,2,3]); // true
fn.isArray("abcde"); // false
```

##5.2. Angular
###5.2.1. Service
####$func
#####post
Melakukan request POST AJAX.

###5.2.2. Filter
####isEmpty
Mengecek suatu variable apakah kosong atau tidak. Pengecekan dilakukan 

##5.3. DOM Event
Akan sangat merepotkan jika kita harus menuliskan event javascript pada setiap halaman. Dan akan sangat tidak efisien apabila event tersebut sudah pernah kita buat sebelumnya. Oleh karena itu, framework ini menyediakan event-event javascript untuk anda pakai. Anda tidak perlu lagi menuliskan javascript pada setiap halaman yang anda temui. Cukup tambahkan selector dan attribut yang dibutuhkan saja.

###5.3.1. Input
####Select2

```html
<select select2>
    <option value="1">Opt 1</option>
    <option value="2" selected>Opt 2</option>
</select>
```

#####Multiple Select

```html
<select select2 multiple>
    <option value="1" selected>Opt 1</option>
    <option value="2" selected>Opt 2</option>
</select>
```
>https://select2.github.io/

####Datepicker

```html
<input type="text" datepicker>
```

####Date Range

```html
<input type="text" datepicker-range>
```
>http://bootstrap-datepicker.readthedocs.org/en/stable/

####Timepicker

```html
<input type="text" timepicker>
```

####Toggle Switch

```html
<input type="checkbox" switch>
```

####Number Format

```html
<input type="text" num-format>
```

###5.3.2 Modal
####5.3.2.1. Membuat modal
Untuk membuat modal javascript anda perlu menambahkaan htmlnya sendiri ke dalam view. Bentuk html itu sendiri sama seperti html untuk membuat modal dengan menggunakan bootstrap. Karena memang bootstrap yang digunakan :3.
Jika menuliskan htmlnya terlalu panjang bagi anda atau anda lupa susunan html-nya, anda bisa tuliskan seperti ini.

```php
include("ui.modal", [
    "id" => "my-modal-selector",
    "class" => "modal-sm",
    "attr" => ["style" => "font-weight: bold"],
    "title" => "Judul Modal",
    "body" => "<div>Content.</div>",
    "footer" => 
        '<button type="button" data-dismiss="modal" class="btn btn-default">Tidak</button>
        <button type="button" class="btn btn-primary modal-accept">Ya</button>',
])
```

Jika anda tidak memerlukan salah satu dari index diatas, anda tidak harus mengisinya dengan `null` atau string kosong `""` cukup hiraukan saja. Karena tidak ada satupun yang bersifat required.

####5.3.2.2. Pengunaan Modal
#####a. Basic
Untuk menggunakan modal yang sudah anda buat, anda cukup menambahkan attribut `modal` dan tentukan selector element modal yang akan dieksekusi. Berikut contoh penggunaannya.

```html
<button modal='#my-modal-selector'>Show Modal</button>
```

Ketika anda meng-klik tombol tersebut, modal dengan id `my-selector-modal` akan ditampilkan. Apabila anda tidak mengatur value pada attribut modal, secara default valuenya adalah `#modal-basic`.

#####b. Mengatur Title Modal
Title pada modal akan otomatis menggunakan title halaman yang sedang anda akses. Jika anda ingin menggunakan title yang lain, gunakan attribut `modal-title`.

```html
<button modal='#my-modal-selector' modal-title='Judul Modal'>Show Modal</button>
```

#####c. Mengatur Body Modal
Body pada modal bisa anda atur dengan attribut `modal-body`.

```html
<button modal='#my-modal-selector' modal-body='Badan Modal'>Show Modal</button>
```

#####d. Ukuran Modal
Modal bootstrap menyediakan tiga ukuran pada modal, yaitu `sm`, `md`, dan `lg`. Untuk menentukan ukuran modal yang ingin anda tampilkan anda bisa gunakan seperti ini :

```html
<button modal-sm='#my-modal-selector'>Show Modal</button>
```

#####e. Modal AJAX
Jika anda menambahkan attribut modal pada element `a` yang memiliki attribute `href`, content modal yang ditampilkan pada saat anda meng-kliknya adalah content halaman url pada attribute href tersebut. 

```html
<a href="url-content" modal="#modal-basic">Show Modal</button>
<!-- atau kosongkan nilai dari attribute 'modal'. Karena secara default akan mengarah ke #modal-basic -->
```
Pada saat modal melakukan request ajax pada url tersebut, aplikasi akan merespon dengan kode HTML yang mendeskripsikan halaman secara keseluruhan. Sayangnya hal tersebut tidak dibutuhkan. Kita hanya butuh kontennya saja, tanpa header, sidebar, ataupun footer. Untuk menyiasati hal tersebut, anda bisa atur pada source view yang diakses pada halaman tersebut seperti ini.


```html
extends('app')

section('content')
    <div class="panel">
        <div class="panel-heading">
            <h2 class="panel-title">Contoh Transaction</h2>
        </div>

        <div class="panel-body">
        @section("content-ajax")
            {!! Form::model(@$model) !!}
                {!! Form::group('text', 'amount', 'Amount') !!}
                {!! Form::group('select', 'provinsi_id', 'Provinsi', Provinsi::lists("name", "id")) !!}
                {!! Form::group('select', 'kota_id', 'Kota', Kota::lists("name", "id")) !!}
                {!! Form::group('textarea', 'description', 'Description') !!}

                <div class="col-md-offset-3">
                    {!! Form::submit("Simpan", ["name" => "save"]) !!}
                    {!! Form::submit("Kirim", ["name" => "sent", "class" => "btn btn-warning"]) !!}
                </div>
            {!! Form::close() !!}
        @show
        </div>
    </div>
endsection

```

Jika anda mendefinisikan section `content-ajax` pada view, aplikasi akan merespon dengan kode HTML yang berada pada section tersebut. Jika anda tidak mendefinisikannya, respon aplikasi akan mengambil dari section `content`. Jadi anda tidak perlu repot-repot memisah file view atau memfilternya di javascript.
>Catatan: Hanya berlaku pada request ajax.

####5.3.2.3. Predefined Modal
Pada framework ini sudah disediakan beberapa modal sebagai berikut.

#####a. Modal Basic
Modal ini merupakan modal default yang akan digunakan jika anda tidak mengatur selector pada attribut modal. Modal ini hanya menampilkan header dan body saja, tanpa footer. Modal ini digunakan secara default digunakan untuk menampilkan pesan alert atau content yang di-load oleh AJAX.

```html
<a href="url-content" modal="#modal-basic">Show Modal</a>
```

#####b. Modal Confirm
Jika anda membutuhkan konfirmasi action terhadap user, anda bisa menggunakan modal ini. Modal ini disediakan footer dengan dua tombol. Ya dan Tidak.

```html
<a href="delete/12" confirm="Apakah anda yakin ?">Hapus</a>
```

#####c. Modal Error
Anda bisa menggunakan modal ini untuk menampilkan pesan error yang terjadi pada javascript. Modal ini menyediakan tombol untuk close dan mereload ulang halaman.

###5.3.3. Datatable
####a. Basic
Untuk membuat datatable, anda cukup menambahkan saja attribute `datatable` pada element `table`. Berikut contohnya.

```html
 <table class="table table-bordered" datatable dt-paginate="false">
     <thead>
         <tr>
             <th>ID</th>
             <th>Amount</th>
             <th>Created At</th>
             <th>Updated At</th>
         </tr>
     </thead>
     <tbody>
        <tr>
             <td>Value of ID</td>
             <td>Value of Amount</td>
             <td>Value of Created At</td>
             <td>Value of Updated At</td>
        </tr>
     </tbody>
 </table>
```
Pada contoh diatas anda dapat melihat attribut `dt-paginate="false"`. Attribut tersebut akan mendisable fitur pagination pada datatable. Jika anda ingin mengaktifkannya, atur nilainya menjadi `true` atau hapus attribut tersebut. Karena fitur pagination akan aktif secara default.

####b. Datatable AJAX
Berikut contoh pembuatan AJAX Datatable.

######Server Side

```php
use Datatables;
//...

// Simple
public function anyTransactionData($status) {
    // membuat respon datatable
    return Datatables::of(
        DB::table("transaction")->where("status", $status)
    )->make(true);
}

// Dengan custom data
public function anyTransactionData($status) {
    // membuat respon datatable
    $res = Datatables::of(
        DB::table("transaction")->where("status", $status)
    )->make(true);

    // custom content
    $content = $res->getData();
    $content->data = array_map(function($data) use ($status) {
        $data->action = "custom action button";

        return $data;
    }, $content->data);

    return $res->setData($content);
}
```
Pada sisi server, gunakan facade `Datatables` untuk membuat response AJAX. Anda tidak perlu mengkhawatirkan tentang searching, sorting maupun paging pada data. 
>Untuk lebih detail penggunaan datatable, anda bisa kunjungi halaman berikut. https://github.com/yajra/laravel-datatables/wiki

######Client Side
Setelah anda membuat halaman untuk merespons request datatable, tambahkan url yang mengarah pada halaman tersebut pada attribute `datatable`. Lalu berikan pula attribute `dt-field` dengan nilainya adalah nama index pada data yang anda berikan pada response. Biasanya adalah nama kolom pada table. Anda pun bisa mendisable fitur sorting dan pencarian jika perlu. Cukup tambahkan attribute `sort` dan `search` dengan nilai `false`.

```html
 <table class="table table-bordered" datatable="{!! url("contoh-transaction/transaction-data/draft") !!}">
    <thead>
        <tr>
            <th dt-field="id">ID</th>
            <th dt-field="amount">Amount</th>
            <th dt-field="created_at">Created At</th>
            <th dt-field="updated_at">Updated At</th>
            <th dt-field="action" sort="false" search="false">Actions</th>
        </tr>
    </thead>
 </table>
```

###5.3.4. Tooltip

```html
<a href='url' tooltip='View' data-placement="left">View</a>
```
>http://getbootstrap.com/javascript/#tooltips

#6. Validasi
##6.1. Form Builder
Validasi pada form input menggunakan form builder memiliki konsep yang sama dengan validasi client yang sudah dijelaskan pada bagian DOM event. Kita perlu menambahkan attribute `rules` pada form input disertai dengan rules nya. 

```php
{!! Form::group('text', 'number', 'Number', null, ["rules" => "integer|min:8"]) !!}

// atau

{!! Form::text('number', null, ["rules" => "integer|min:8"]) !!}
```

###6.1.1. Model
Jika anda sudah menuliskan rules pada model, akan sangat mudah dalam menggunakannya. 

```php
{!! Form::model($model /* atau bisa juga MyModel::class */) !!}
// form input..
{!! Form::close() !!}
```

Pada method model, anda bisa menginputkan instance model anda. Atau jika anda ingin menghemat memory, bisa juga memasukkan namespace model anda.

###6.1.2. Rules
Bisa juga ditambahkan manual seperti berikut.

```php
{!! Form::open() !!}

{!! Form::rules(MyModel::$rules) !!}
// atau
{!! Form::rules(["number" => "required|integer|max:30"]) !!}

// form input..

{!! Form::close() !!}
```


##6.2. Model
###6.2.1. Rules
Memvalidasi data pada model hanya perlu menambahkan attribute `$rules`.

```php
class TableName extends \App\Models\Model
{
    public $fillable = ["amount", "kota_id", "description"];
    public $table = "table_name";
    
    public static $rules = [
        "amount" => "integer",
        "kota_id" => "required|integer",
        "description" => "required",
    ];
}
```

Proses validasi akan dilakukan saat model menyimpan data. Yaitu pada saat fungsi `save` dipanggil. Jika data yang divalidasi tidak valid, maka ia akan mengembalikan nilai `false`.

###6.2.2. BeforeSave
Jika anda ingin lebih leluasa dalam memvalidasi data, anda bisa gunakan method `beforeSave`. 

```php
class User extends \App\Models\Model {

    public function beforeSave() {
        // if there's a new password, hash it
        if ($this->isDirty('password')) {
            $this->password = Hash::make($this->password);
        }

    return true;
    // or don't return anything, since only a boolean false will halt the operation
    }
}
```

###6.2.3. Validasi Static
Anda bisa memvalidasi data tanpa harus membuat instance model dengan method `validateData`. 

```php
// return boolean
MyModel::validateData([
    "title" => "xxxxx",
    "harga" => "bukan angka"
]); 
```

Data yang divalidasi akan diperika sesuai dengan rule yang sudah dideklarasikan. Method ini tidak akan menyimpan data apapun, ia hanya berfungsi untuk mengecek apakah data valid atau tidak.

###6.2.4. Validasi Input Data dengan Pesan Error
Saat anda memvalidasi data yang diinputkan oleh user melewati form HTML, ada saatnya kita ingin meredirect ke halaman form HTML kembali untuk menampilkan error validasi. Untuk melakukan hal tersebut, secara manual anda bisa melakukan throw exception dengan class `App\Exceptions\ValidatorException` disertai dengan validator yang dibuat. Jika itu terlalu merepotkan, anda bisa gunakan salah satu method berikut.

####a. createOrFail

```php
MyModel::createOrFail($data);
```

####b. updateOrFail

```php
$model = MyModel::find($id);
$model->updateOrFail($data);
```

####c. saveOrFail

```php
/*
find model
$model = MyModel::find($id);

or create a new model
$model = new MyModel

update field
$model->field = $field_data;
*/

$model->saveOrFail();
```

###6.2.5. Referensi
>https://github.com/laravel-ardent/ardent
>https://laravel.com/docs/5.2/validation#available-validation-rules

##6.3. Middleware
###6.3.1. HashID
Mengecek kecocokan antara id dan hashid.

```php
Route::group(['middleware' => ["validate.id"]], function () {
    Route::controller("app", "AppController");
    
    Route::validateDataId([
        "app/edit/{id}/{hashid}",
        "app/delete/{id}/{hashid}",
        "app/view/{id}/{hashid}",
    ]);
});
```