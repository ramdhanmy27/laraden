var elixir = require('laravel-elixir');
var laraden = require("./vendor/ramdhanmy27/laraden-framework/resources/laraden-elixir")(elixir)

elixir.config.sourcemaps = false;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

require('dotenv').config()
laraden.theme(process.env.MIX_THEME)

/** Core Assets */
// elixir(function(mix) { });