<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Compiler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'compile-me {--app=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compile applications definitions to "definitions.json"';

    /**
     * Definitions container.
     * @var array
     */
    private $definitions = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $apps_path           = [];
        $options        = [];
        $options['app'] = $this->option('app');

        // Apps to crawl
        if ($options['app'] !== null)
            $apps_path[] = 'Apps/' . $options['app'] . '/definitions.php';
        else
        {
            // Get Apps path
            $path = app_path('Apps/*/definitions.php');

            // List all files in module path
            $apps_path = glob($path);
        }

        if (count($apps_path) < 1)
            return $this->info('Cannot find any \'definitions\' file.');

        // Compile files
        $this->compile($apps_path);
    }

    /**
     * Read all definitions files
     * and compile it to json.
     * 
     * @param  array $apps_path      applications path
     * @return void
     */
    private function compile($apps_path)
    {
        foreach ($apps_path as $app)
        {
            // File
            $defines = require $app;
            $code = str_replace(base_path(), '', $app);
            $code = str_replace('definitions.php', '', $code);
            $code = str_replace('/', '\\', $code);
            $code = strtolower(trim($code, '\\'));

            $this->definitions[$code] = $defines;
        }
        
        $new_def = [
            'id' => date('YmdHis'),
            'apps' => [],
        ];


        $new_def['apps'] = $this->definitions;

        $has_changes = true;

        $path = base_path('definitions.json');
        if (file_exists($path))
        {
            $old_def = file_get_contents($path);
            $old_def = json_decode($old_def, true);

            $has_changes = $this->hasChanges($old_def, $new_def);
        }
        
        if (! $has_changes)
        {
            $this->info('No changes.');
            return true;
        }

        // Check if dependencies available
        foreach ($new_def['apps'] as $key => $def)
        {
            // Independent app
            if (! isset($def['require']))
                continue;

            $append = false;
            foreach ($def['require'] as $dep)
            {
                $pass = true;
                if (! file_exists( str_replace('\\', '/', $dep) ) )                
                    $pass = false;

                if (! isset($new_def['apps'][strtolower($dep)]))
                    $pass = false;

                if (! $pass)
                {
                    $this->error("Cannot find '" . $dep ."' as dependency of '" . $def['name'] . "'");
                    return false;
                }
            }
        }

        $is_success = file_put_contents($path, json_encode($new_def)) !== false;
        
        if ($is_success)
            $this->info('Compiled to definitions.json');

        return $is_success;
    }

    /**
     * Determine if definitions has changes
     * compare to old definitions.
     * 
     * @param  array  $old_def      old definitions from json file
     * @param  array  $new_def      new definitions
     * @return boolean
     */
    private function hasChanges($old_def, $new_def)
    {
        if (count($old_def['apps']) != count($new_def['apps']))
            return true;

        foreach ($new_def['apps'] as $key => $def)
        {
            if (! isset($old_def['apps'][$key]))
                return true;

            if ($new_def['apps'][$key]['name'] != $old_def['apps'][$key]['name'])
                return true;

            if ($new_def['apps'][$key]['version'] != $old_def['apps'][$key]['version'])
                return true;
        }

        return false;
    }
}
