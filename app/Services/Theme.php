<?php

namespace App\Services;

use Controller;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;
use View;

class Theme {

    const DIR_PATH = "theme";

    /**
     * get asset url depend
     * @param  string  $path
     * @param  boolean $secure
     * @return string
     */
    public function asset($path, $secure = null) {
        // asset path with theme defined
        $pos = strpos($path, "::");

        // set asset url to theme asset url
        if ($pos !== false)
            $path = self::DIR_PATH."/".substr($path, 0, $pos)."/".substr($path, $pos+2);

        return app('url')->asset($path, $secure);
    }

    /**
     * get default view path
     * @param  string $view
     * @return string
     */
    public function defaultPath($view = null) {
        $path = (!APP_MATCHED ?: "app::").str_replace("\\", ".", config("controller.id"));

        if ($view === ".")
            return $path;
        else if ($view === null)
            return $path.".".config("controller.action");
        else
            return $path.".$view";
    }

    /**
     * render view depend
     * @param  string $view
     * @param  array  $data
     * @param  array  $merge
     * @return View
     */
    public function view($view = null, $data = [], $merge = []) {
        $factory = app(Factory::class);

        if (func_num_args() === 0 || $view === null)
            return $factory;

        // no view name provided
        if (is_array($view))
            return $factory->make($this->defaultPath(), $view, $data);
        else
            return $factory->make($view, $data, $merge);
    }

    /**
     * check view existence
     * @param  string $path
     * @return boolean
     */
    public function viewExists($path) {
        $view_path = "views";

        // namespace included
        $pos = strpos($path, "::");

        if ($pos === false) {
            // set view path to theme path
            $theme = substr($path, 0, $pos);
            $view_path = self::DIR_PATH."/".$theme."/".$view_path;
        }

        return file_exists("resources/$view_path/".str_replace(".", "/", $path).".blade.php");
    }
}