<?php

namespace App\Services\Routing;

use Menu;

class Route extends \Illuminate\Routing\Route {
	
	/**
	 * Register Menu with current route
	 * 
	 * @param  String $title   
	 * @param  String $icon
     * @return \Illuminate\Routing\Route
	 */
	public function menu($title, $icon = null, $arg = []) {
        return Menu::driver("router")->add([
        	"title" => $title, 
        	"url" => self::bindParam($this->domain().$this->uri, $arg), 
        	"icon" => $icon,
        	"feature" => \Route::getFeature(),
        ]);
	}

	/**
	 * Get rendered route
	 * 
	 * @param  String $url
	 * @param  Array|Closure  $data
	 * @return String
	 */
	public static function bindParam($url, $arg) {
		return preg_replace_callback(
			'/\{(.*?)\}/',
			!is_array($arg) ? $arg : function($match) use (&$arg) {
	        	$val = array_shift($arg);
	        	return $val===null ? $match[0] : $val;
	        }, 
	        $url
	    );
	}
}