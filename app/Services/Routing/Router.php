<?php

namespace App\Services\Routing;

use Closure;
use Menu;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Container\Container;
use App\Services\Routing\Router;
use App\Services\Widget\Menu\MenuRouting;

class Router extends \Illuminate\Routing\Router {

    /**
     * Containing mixed data
     * 
     * @var Object
     */
	private $param;

	public function __construct(Dispatcher $events, Container $container = null) {
        parent::__construct($events, $container);

        $this->routes = new RouteCollection;
        $this->menu = new MenuRouting;
        $this->param = new \StdClass;
    }

    /**
     * Register menu with an url route
     * @param  array        $attr
     * @param  Closure|null $action
     * @return App\Services\Widget\Menu\Factory\Router
     */
    public function menu($attr, Closure $action = null) {
            if (is_string($attr))
                $attr = ["title" => $attr];

        if ($action === null)
            $menu = Menu::driver("router")->add($attr);
        else 
            $menu = Menu::driver("router")->addGroup($attr, function() use ($attr, $action) {
                $this->group($attr, $action);
            });

        return $menu;
    }
	
    /**
     * Add a route to the underlying route collection.
     *
     * @param  array|string  $methods
     * @param  string  $uri
     * @param  \Closure|array|string  $action
     * @return \Illuminate\Routing\Route
     */
    protected function addRoute($methods, $uri, $action)
    {
        return $this->routes->addItem($this->createRoute($methods, $uri, $action));
    }

	/**
     * Create a new Route object.
     *
     * @param  array|string  $methods
     * @param  string  $uri
     * @param  mixed   $action
     * @return \Illuminate\Routing\Route
     */
    protected function newRoute($methods, $uri, $action)
    {
        return (new Route($methods, $uri, $action))
                    ->setRouter($this)
                    ->setContainer($this->container);
    }

    /**
     * Route a controller to a URI with wildcard routing.
     *
     * @param  string  $uri
     * @param  string  $controller
     * @param  array  $names
     * @return void
     *
     * @deprecated since version 5.2.
     */
    public function controller($uri, $controller, $names = []) {
        parent::controller($uri, $controller, $names);
        $this->menu->setPrefix($this->prefix($uri));

        return $this->menu;
    }

    /**
     * Register Administration route
     * 
     * @return MenuRouting
     */
    public function admin() {
        return $this->menu(["title" => "Administrator", "icon" => "fa fa-cogs"], function() {
            $this->group(["prefix" => "admin", "namespace" => "Admin"], function() {
                // Roles
                $this->controller("role", "RoleController")->menu("Roles", "fa fa-user");
 
                // Users, Permissions
                $this->controller("/", "AdminController")
                    ->menu("Users", "fa fa-users")
                    ->menu("Permissions", "fa fa-lock", "permissions");
 
                $this->validateDataId([
                    "edit/{id}/{hashid}",
                    "delete/{id}/{hashid}",
                    "view/{id}/{hashid}",
 
                    "role/edit/{id}/{hashid}",
                    "role/delete/{id}/{hashid}",
                    "role/view/{id}/{hashid}",
                ]);
            });
        });
    }

    public function validateDataId($uri) {
        $this->param->uriValidate = [];

        foreach (is_array($uri) ? $uri : func_get_args() as $url)
            $this->param->uriValidate[] = $this->prefix($url);
    }

    public function getParam() {
        return $this->param;
    }
}