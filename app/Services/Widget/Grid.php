<?php

namespace App\Services\Widget;

use Controller;
use DB;
use DataGrid;
use Html;
use Illuminate\Database\Eloquent\Builder;

class Grid {

	const USE_HASHID = true;

	/**
	 * Initialize DataGrid instance
	 * 
	 * @param  Illuminate\Database\Query\Builder  $query
	 * @param  array  $field   
	 * @param  integer $paginate
	 * @return DataGrid
	 */
	public function init($query, $field = null, $paginate = 100) {
		$grid = DataGrid::source($query);

        // set up table columns
        if (is_array($field) && is_assoc($field)) {
            foreach ($field as $col => $label)
                $grid->add($col, $label);
        }
    	// query builder input
        else {
	        if ($field === null) {
	        	if ($query instanceof Builder)
	        		$query = $query->getQuery();

	            $field = isset($query->columns) ? $query->columns : \Schema::getcolumnListing($query->from);
	        }
	        
            foreach ($field as $field)
                $grid->add($field, id2title($field, "_"));
        }

        // pagination
        if (is_numeric($paginate) && $paginate > 0 && $query->count()>$paginate)
            $grid->paginate($paginate);

        return $grid;
	}

	/**
	 * Initialize DataGrid instance with model instance
	 * 
	 * @param  String|Class  $model
	 * @param  array  $field   
	 * @param  integer $paginate
	 * @return DataGrid
	 */
	public function model($model, $field = null, $paginate = 100) {
		if (!is_string($model))
			$model = get_class($model);

		return $this->init($model::query(), $field, $paginate);
	}

	/**
	 * Create DataGrid instance
	 * 
	 * @param  String  $table
	 * @param  Builder|array  $source
	 * @param  integer $paginate
	 * @param  string  $primaryKey
	 * @return DataGrid
	 */
	public function table($table, $source = null, $paginate = 100, $primaryKey = "id") {
        // select all columns (default)
        if ($source === null) {
            $query = DB::table($table);
            $source = \Schema::getcolumnListing($table);
        }
    	// something else
    	else if (!is_array($source))
			$source = [$source];

        if (!isset($query))
            $query = DB::table($table)->select($source);

        $query->addSelect($primaryKey);

        return $this->init($query, $source, $paginate);
	}

	public function action($model, $base_url = "", array $buttons = [], $template = "view|edit|delete") {
		if (is_numeric($base_url) || !empty($base_url))
			$base_url .= "/";

		$buttons += [
			"view" => function($id, $model) use ($base_url) {
				return Html::link(
					"{$base_url}view/$id".(self::USE_HASHID ? "/".md5($id) : ""), 
					Html::icon("fa fa-eye text-info")
				);
			},
			"edit" => function($id, $model) use ($base_url) {
				return Html::link(
					"{$base_url}edit/$id".(self::USE_HASHID ? "/".md5($id) : ""), 
					Html::icon("fa fa-pencil")
				);
			},
			"delete" => function($id, $model) use ($base_url) {
				return Html::link(
					"{$base_url}delete/$id".(self::USE_HASHID ? "/".md5($id) : ""), 
					Html::icon("fa fa-trash-o text-danger"),
					["method" => "POST", "confirm" => "Apakah anda yakin ?"]
				);
			},
		];

		$id = $model->getKey();
		$result = "";

		foreach (explode("|", $template) as $act) {
			$act = explode(":", $act);
			$result .= " ".$buttons[$act[0]]($id, $model);
		}

		return $result;
	}
}