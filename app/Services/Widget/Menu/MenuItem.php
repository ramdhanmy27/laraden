<?php

namespace App\Services\Widget\Menu;

class MenuItem {

	public $parent, $title, $url, $icon, $isValid, $feature;
	public $active = false;

	public function __construct(array $param) {
		$this->parent = @$param["parent"];
		$this->title = @$param["title"];
		$this->url = removeSlash(@$param["url"]);
		$this->icon = @$param["icon"];
		$this->isValid = @$param["isValid"];
		$this->feature = @$param["feature"];
	}

	public function on($callback) {
		$this->isValid = $callback;
	}
}