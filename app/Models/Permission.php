<?php 

namespace App\Models;

use App\Http\Controllers\Controller;
use Cache;
use DB;
use Storage;

class Permission extends \Zizaco\Entrust\EntrustPermission {

	/**
	 * register permission(s) to database
	 * 
	 * @param  array $permission 
	 * e.g [["name"=>"perm-name1", "description"=>"description1"] 
	 *	   ["perm-name2", "description2"]]
	 * @return int "affected rows"
	 */
	public function batch_register(array $permissions) {
		$data = [];
		$time = DB::raw("NOW()");

		foreach ($permissions as $perm) {
			$name = isset($perm['name']) ? $perm['name'] : current($perm);

			$data[] = [
				"name" => $name,
				"description" => isset($perm['description']) ? $perm['description'] : next($perm),
				"display_name" => ucwords(preg_replace("/[\-\_]+/", " ", substr($name, strrpos($name, ":")+1))),
				"created_at" => $time,
				"updated_at" => $time,
			];
		}

		return DB::table("permissions")->insert($data);
	}

	/**
	 * update permissions descripton
	 * 
	 * @param  array $permission e.g [["permName", "description"]]
	 * @return int "affected rows"
	 */
	public function batch_update($permission) {
		$values = [];

		foreach ($permission as $val)
			$values[] = "('".implode("','", $val)."')";

		DB::update(
			"UPDATE permissions p set description=t.description from (values ".implode(",", $values).") as t (name, description)
			where t.name::varchar=p.name"
		);
	}

	/**
	 * remove permissions from database
	 * 
	 * @param  string|array $permission e.g ["permName1", "permName2"]
	 * @return int "affected rows"
	 */
	public function batch_remove($permission) {
		if (!is_array($permission))
			$permission = [$permission];

		return DB::table("permissions")
				->whereIn("name", $permission)
				->delete();
	}

	/*Controllers*/

	/**
	 * clear permissions cache and re-register all controller permissions
	 * 
	 * @param  array  $dir ["namespace\path" => "@folder/path"]
	 */
	public function initAllController(array $dir) {
		foreach ($dir as $namespace => $path) {
			$files = Storage::disk("app")->allfiles($path);

			foreach ($files as $file) {
				if (!preg_match("/.+Controller\.php$/", $file))
					continue;

				$strstart = strpos($file, $path)+strlen($path)+1;
				$classname = str_replace("/", "\\", substr($file, $strstart, strrpos($file, ".php")-$strstart));
				$class = $namespace."\\".$classname;

				if (class_exists($class) && isset($class::$permission) && method_exists($class, "id")) {
					$id = $class::id();
					
					Cache::forget("$id|cp");
					$this->initController($id, $class::$permission);
				}
			}
		}
	}

	/**
	 * checking for permissions update and registering permissions
	 * 
	 * @param  string  $id
	 * @param  array  $permissions
	 */
	public function initController($id, array $permissions) {
		if (!is_array($permissions))
			return;

		$do_update = false;
		$key = "$id|cp";

		// get current permissions from cache | database (if cache is not available)
		$current = Cache::has($key) ? Cache::get($key) : $this->getByController($id);
		$new = $this->parse($permissions, $id);

		$update_diff = [];

		// insert new permissions
		$insert_diff = array_diff_ukey($new, $current, function($x, $y) use ($current, $new, &$update_diff) {
			if ($x == $y) {
				// update description
				if ($current[$x]['description'] != $new[$x]['description'])
					$update_diff[] = $new[$x];

				return 0;
			}
			else return $x > $y ? 1 : -1;
		});

		// update description
		if (count($update_diff) > 0) {
			$this->batch_update($update_diff);
			$do_update = true;
		}

		// create new permissions
		if (count($insert_diff) > 0) {
			$this->batch_register($insert_diff);
			$do_update = true;
		}

		// remove old permissions
		$delete_diff = count($current)>0 ? array_diff_key($current, $new) : [];

		if (count($delete_diff) > 0) {
			$this->batch_remove(array_keys($delete_diff));
			$do_update = true;
		}

		// update cache
		if ($do_update === true)
			Cache::forever($key, $this->getByController($id));
	}

	/**
	 * parsing data permissions from controller
	 * 
	 * @param  array  $permissions 
	 * @param  string $controller  "controller name id"
	 * @return array
	 *
	 * $permissions Format
	 * [["perm-name", "description"]]
     * [["perm-name"]]
     * ["perm-name"]
	 */
	public function parse($permissions, $controller) {
		$data = [];

		foreach ($permissions as $perm) {
			$desc = null;

			if (is_array($perm)) {
				// skip empty array
				if (count($perm)==0)
					continue;

				@list($perm, $desc) = $perm;
			}
			else if (empty($perm) || trim($perm)=='')
				continue;

			// Set default description
			if (!isset($desc))
				$desc = ucwords($perm." ".id2title($controller));

			$perm = $this->getPermissionName($controller, $perm);
			$data[$perm] = ["name" => $perm, "description" => $desc];
		}

		return $data;
	}

	/**
	 * get formatted permission name i.e. controller-id:permission
	 * 
	 * @param  string $permission
	 * @param  string $controller 	"controller name id"
	 * @return string
	 */
	public function getPermissionName($controller, $permission) {
		return $controller.':'.$permission;
	}

	/**
	 * Get permission name from current accessed controller
	 * 
	 * @param String $permission
	 */
	public function Controller($permission) {
		return $this->getPermissionName(Controller::$app["id"], $permission);
	}

	/**
	 * get all permissions on specific controller
	 * 
	 * @param  string $controller "controller name id, will use current controller if its empty"
	 * @return array
	 */
	public function getByController($controller) {
		return $this->whereRaw("name ~* E'^".str_replace("\\", "\\\\\\\\", $controller).":.+'")
					->select("name", "description")
					->get()->keyBy("name")->toArray();
	}
}