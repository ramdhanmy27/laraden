<?php 

namespace App\Models\Auth;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use \Zizaco\Entrust\Traits\EntrustUserTrait;
}