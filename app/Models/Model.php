<?php

namespace App\Models;

use App\Exceptions\ValidatorException;
use LaravelArdent\Ardent\Ardent;
use Validator;
use Closure;

class Model extends Ardent {

	public $label;

	public function __construct(array $attributes = []) {
        parent::__construct($attributes);

        if (isset(static::$labels) && count(static::$labels) > 0)
	        $this->label = (object) static::$labels;
    }

	/**
	 * @inheritDoc
	 */
    public static function createOrFail(array $data = []) {
        $model = new static($data);
        $model->saveOrFail();

        return $model;
    }

	/**
	 * @inheritDoc
	 */
    public function updateOrFail(array $data = [], array $opt = []) {
        if (!$this->exists)
            return false;

        return $this->fill($data)->saveOrFail([], [], $opt);
    }

    /**
     * Save the model to the database using transaction.
     *
     * @param  array  $options
     * @return bool
     *
     * @throws \Throwable
     */
    public function saveOrFail(array $rules = [], array $msg = [], array $opt = [], Closure $before = null, Closure $after = null) {
    	$success = $this->save($rules, $msg, $opt, $before, $after);

    	if (!$success)
			throw new ValidatorException($this->validator);

		return $success;
    }

	/**
	 * Validate data input
	 * 
	 * @param  array $data
	 * @return boolean
	 */
	public static function validateData(array $data, array $rules = [], array $customMessages = [], array $customAttributes = []) {
		$validator = static::makeValidator($data, 
						empty($rules) ? static::$rules : $rules, 
						empty($customMessages) ? static::$customMessages : $customMessages, 
						empty($customAttributes) ? static::$customAttributes : $customAttributes
					);

		return $validator->passes();
	}

	public function getRules() {
		return isset(static::$rules) ? static::$rules : null;
	}
}
