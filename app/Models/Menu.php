<?php

namespace App\Models;

class Menu extends Model {
	public $table = "menu";
	protected $fillable = ["title", "url", "parent", "enable", "order"];
	public $timestamps = false;
}
