<?php 

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use \Zizaco\Entrust\Traits\EntrustUserTrait;

    protected $fillable = ['name', 'email', 'password'];
    protected $hidden = ['password', 'remember_token'];

    public static $rules = [
        "name" => "required",
    	"email" => "required|email",
    ];

    public function scopeSearch($query, $value)
    {
        return $query->where("name", "like", "%$value%")
                    ->orWhere("email", "like", "%$value%");
    }

    public function registerRoles(array $roles) {
        $data = [];
        $id = $this->getKey();

        foreach ($roles as $role_id)
            $data[] = ["role_id" => $role_id, "user_id" => $id];

        DB::table("role_user")->where("user_id", $id)->delete();
        DB::table("role_user")->insert($data);
    }
}