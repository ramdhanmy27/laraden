<?php

namespace App\Listeners;

class ModuleListener {
	
	public function handle($event) {
		$app_id = $event->request->segment(1);
		$app_class = id2camel($app_id, "-");
		$app_path = app_path("Apps/".$app_class);

        // define state that app was accessed
        define("APP_MATCHED", file_exists($app_path."/routes.php"));

		/** Add app view path to view finder */
		if (APP_MATCHED) {
			define("APP_ID", $app_id);
			define("APP_CLASS", $app_class);
			define("APP_PATH", "app\\apps\\".strtolower($app_class));

			// register app view to view finder
			$view_path = $app_path."/views";

            \View::addLocation($view_path);
            \View::addNamespace("app", $view_path);

            // load helpers
            $helper_path = $app_path."/Helpers";

            if (file_exists($helper_path)) {
            	foreach (glob($helper_path."/*.php") as $filepath) {
            		require_once $filepath;
            	}
            }

            // switch default connection to app connection
            config(["database.default" => APP_ID]);
		}
	}
}