<?php

namespace App\Exceptions;

use App\Exceptions\ServiceException;
use App\Exceptions\ValidatorException;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        ServiceException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        switch (get_class($e)) {
            case ServiceException::class:
                return $e->response();

            case ValidatorException::class:
                return back()->withInput()
                        ->withErrors($e->getValidator())
                        ->withErrors($e->getMessages());
            
            default:
                if ($e instanceof HttpException) {
                    $status = $e->getStatusCode();
                    $view = "errors.{$status}";

                    if (view_exists($view))
                        return response()->view(
                            $view, ['exception' => $e], $status, $e->getHeaders()
                        );
                }

                return parent::render($request, $e);
        }
    }
}
