<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\Routing\Route;

class ValidateDataId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current = ($request->getpathinfo());
        $param = $request->route()->parameters();

        // if missing param is exists
        if (isset($param["_missing"]))
            $param = $param["_missing"];

        // iterate uri validate
        foreach (\Route::getParam()->uriValidate as $route) {
            $bindData = [];

            // compile url
            $url = Route::bindParam($route, function($match) use (&$param, &$bindData) {
                $val = array_shift($param);
                $bindData[$match[1]] = $val;

                return $val===null ? $match[0] : $val;
            });

            // route match
            if (strpos($current, $url) !== false) {
                // route has id & hashid parameter
                if (isset($bindData["id"]) && isset($bindData["hashid"])) {
                    // hashid validation
                    if (md5($bindData["id"]) == $bindData["hashid"])
                        return $next($request);
                    else
                        return abort(403);
                }
            }
        }

        return $next($request);
    }
}
