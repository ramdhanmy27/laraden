<?php

namespace App\Http\Middleware;

use Illuminate\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Session\Middleware\StartSession as DefaultSession;

class StartSession extends DefaultSession
{
	/**
     * Add the session cookie to the application response.
     *
     * @param  \Symfony\Component\HttpFoundation\Response  $response
     * @param  \Illuminate\Session\SessionInterface  $session
     * @return void
     */
    protected function addCookieToResponse(Response $response, SessionInterface $session)
    {
    	if ($this->usingCookieSessions()) {
            $this->manager->driver()->save();
        }
    }
}