<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Storage;

class Logger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $req = $next($request);

        // User is anonymous
        if (!Auth::check())
            return $req;

        /** Log Access */

        // Make directory on each year
        $dir = APP_ID."/".date("Y");
        Storage::disk("logs")->makeDirectory($dir);

        // write csv file
        $filename = date("Y-m-d").".csv";
        $open = fopen(config("filesystems.disks.logs")."/".$dir."/".$filename);

        fputcsv($open, [
            Auth::user()->id, // User ID
            date("Y-m-d H:i"), // Access Time
            $request->method()." ".$request->url(), // Access URL
        ]);

        fclose($open);

        return $req;
    }
}
