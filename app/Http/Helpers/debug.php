<?php

namespace App\Http\Helpers;

use Illuminate\Support\Debug\Dumper;

class Debug {

	private static $instance;

	private function __construct() {}

	public static function getInstance() {
		if (self::$instance === null)
			self::$instance = new self;

		return self::$instance;
	}

	public function debugPath($index = 1) {
		$trace = debug_backtrace();
		echo $this->renderDebugPath($trace[$index]);
	}

	public function renderDebugPath($trace) {
		if (isset($trace['file']))
			return "<div class='source-line'>"
						.str_replace(base_path().DIRECTORY_SEPARATOR , null, $trace['file'])
						.'<b> line: '.$trace['line']."</b>
					</div>";
	}

	public function defineArgs(array $args) {
		$res = array();

		if (count($args)) {
			foreach ($args as $val) {
				if (is_array($val)) 
					$res[] = '['.implode(', ', $this->defineArgs($val)).']';
				else if (is_object($val)) 
					$res[] = get_class($val);
				else if (is_string($val))
					$res[] = '"'.$val.'"';
				else if ($val == NULL)
					$res[] = 'NULL';
				else
					$res[] = $val;
			}
		}

		return $res;
	}
	
	public function dump() {
	    array_map(function ($x) {
	        (new Dumper)->dump($x);
	    }, func_get_args());
	}

	public function dumpWithPath($param, $index = 1) {
		$this->debugPath($index);
		call_user_func_array([$this, "dump"], $param);
		$this->scripts();
	}

	public function trace() {
		foreach (debug_backtrace() as $obj) {
			$arg = array();
			echo $this->renderDebugPath($obj)
					.@$obj['class'].'->'.$obj['function']
					.'('.implode(', ', $this->defineArgs($obj['args'])).')';
		}

		call_user_func_array([$this, "dump"], func_get_args());
		$this->scripts();
	}

	public function traceWithPath($param, $index = 1) {
		$this->debugPath($index);
		call_user_func_array([$this, "trace"], $param);
	}

	public function scripts() {
		echo <<<"SCRIPTS"
			<style>
				body {
					margin: 0px;
				}
				.source-line {
					color: #ddd; 
					font-family: monospace; 
					padding: 10px; 
					background: #444; 
					width: 100%;
				}
				.source-line b {
					color: #fff;
				}
			</style>

			<script>
				document.addEventListener("click", function(e) {
					if (e.shiftKey === false)
						return;
					
					var compacted = document.querySelectorAll('.sf-dump-compact');

					for (var i = 0; i < compacted.length; i++)
						compacted[i].className = 'sf-dump-expanded';
				}, false);
			</script>
SCRIPTS;
	}
}