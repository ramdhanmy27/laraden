<?php

use App\Http\Helpers\Debug;
use Illuminate\Contracts\View\Factory as ViewFactory;

function camel2id($str, $delimiter = '-') {
	return strtolower(preg_replace(['/[A-Z]/', "/(^-|(\\\\)-)/"], ["$delimiter$0", "$2"], $str));
}

function id2title($str, $delimiter = '-') {
    $pos = strrpos($str, "\\");

    if ($pos !== false)
        $str = substr($str, $pos+1);

    return ucwords(trim(str_replace($delimiter, " ", $str)));
}

function id2camel($str, $delimiter = '-') {
    $namespace = [];

    foreach (explode("\\", $str) as $class)
        $namespace[] = str_replace(" ", "", ucwords(str_replace($delimiter, " ", $class)));

    return implode("\\", $namespace);
}

function view($view = null, $data = [], $merge = []) {
	return app("theme")->view($view, $data, $merge);
}

function view_exists($path) {
    return app("theme")->viewExists($path);
}

function first_view($viewpath, $data = [], $merge = []) {
    foreach (is_array($viewpath) ? $viewpath : [$viewpath] as $path) {
        if (view_exists($path))
            break;
    }

    return view($path, $data, $merge);
}

function can($permission, $and = true) {
    $result = true;
    
    if (!($user = Auth::user()))
        return false;

    if (!is_array($permission))
        $permission = [$permission];

    foreach ($permission as $val) {
        $can = $user->can(app("permission")->getPermissionName(config("controller.id"), $val));
        $result = $and ? $result && $can : $result || $can;
    }

    return $result;
}

/**
 * convert array into xml
 * 
 * @param  array $data
 * @return string
 */
function xml_encode(array $data) {
    $xml = '';

    foreach ($data as $key => $value) {
        // if array is sequential
        if (is_array($value) && array_keys($value)===range(0, count($value)-1)) {
            foreach ($value as $val)
                $xml .= "<$key>".(is_array($val) ? xml_encode($val) : $val)."</$key>";
        }
        else $xml .= "<$key>".(is_array($value) ? xml_encode($value) : $value)."</$key>";
    }

    return $xml;
}

/**
 * Convert angka ke format currency
 * 
 * @param  integer  $value     
 * @param  integer  $decimals  jumlah angka belakang koma
 * @param  string   $currency  format currency
 * @return string
 */
function currency($value, $decimals=0, $currency="Rp") {
    return "$currency ".number_format($value, $decimals, ",", ".");
}

/**
 * Check whether array is associative or not
 * 
 * @param  array   $data
 * @return boolean
 */
function is_assoc(array $data) {
    return range(0, count($data)) === array_keys($data);
}

/**
 * remove forward and trailing slashes
 *  
 * @param  string $url
 * @return string
 */
function removeSlash($url) {
    if ($url === null)
        return null;

    return preg_replace("/^[\/\\\\]*(.+?)[\/\\\\]*$/", "$1", $url);
}

function asset($path, $secure = null) {
    return app("theme")->asset($path, $secure);
}

function dd() {
    Debug::getInstance()->dumpWithPath(func_get_args(), 2);
    exit;
}

function dds() {
    Debug::getInstance()->dumpWithPath(func_get_args(), 2);
}

function trace() {
    Debug::getInstance()->traceWithPath(func_get_args(), 2);
    exit;
}

function traces() {
    Debug::getInstance()->traceWithPath(func_get_args(), 2);
}