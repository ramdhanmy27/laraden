<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Request;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \App\Http\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \App\Http\Middleware\ViewResolver::class,
            \App\Http\Middleware\Logger::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'auth.admin' => \App\Apps\Admin\Middleware\AuthenticateAdmin::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'validate.id' => \App\Http\Middleware\ValidateDataId::class,
        'config' => \App\Http\Middleware\Configurator::class,
        'sass' => \App\Http\Middleware\TenantSass::class,
    ];

    protected function dispatchToRouter()
    {
        $this->router = $this->app['router'];

        foreach ($this->middlewareGroups as $key => $middleware)
            $this->router->middlewareGroup($key, $middleware);
        
        foreach ($this->routeMiddleware as $key => $middleware)
            $this->router->middleware($key, $middleware);

        // List all files in module path
        $path = app_path('Apps');
        $current = \Request::segment(1);
        $is_global_route = true;

        foreach (glob("$path/*", GLOB_ONLYDIR) as $module) {
            $module_name = removeSlash(str_replace($path, '', $module));
            $namespace = "\App\Apps\\" . $module_name. "\Controllers";
            
            // Add module routes if exists
            if (file_exists($module . '/routes.php')) {
                $url = camel2id($module_name, "-");

                if ($is_global_route && $url == $current) {
                    \Menu::driver("router")->enable = true;
                    $is_global_route = false;
                }
                else \Menu::driver("router")->enable = false;

                $this->router->group([
                    'namespace' => $namespace, 
                    "prefix" => $url, 
                    "middleware" => env("APP_DEBUG") ? ["web"] : ["web", "sass"],
                ], function() use($module) {
                    require $module . '/routes.php';
                });
            }
        }

        // global route
        \Menu::driver("router")->enable = $is_global_route;

        $this->router->group(['namespace' => 'App\Http\Controllers'], function () {
            require app_path('Http/routes.php');
        });

        // 404 handler
        $this->router->group(['middleware' => 'web'], function(){
            $this->router->any('{page}', function() {
                if (! auth()->guard(null)->user())
                    return redirect()->away(env('WEBFRONT_LINK') . '/auth/login');

                if (Request::ajax() || Request::wantsJson())
                    return response('not found.', 404);
                else
                    abort(404);
            });
        });

        return parent::dispatchToRouter();
    }
}
