<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * initialize meta data controller 
     * 
     * @param  string  $method  action method
     * @param  array   $param 
     * @return mixed
     */
    public function callAction($method, $param) {
        // build app meta data
        // Generate controller name ID
        $class = get_called_class();

        /** store value into config */
        config([
            // Controller ID
            "controller.id" => camel2id(preg_replace("/([\w\\\\]+?Controllers\\\\|Controller$)/", "", $class)),

            // Action method
            "controller.method" => $method,
 
            // Action ID
            "controller.action" => camel2id(preg_replace("/^get/", "", $method)),
        ]);

        return call_user_func_array([$this, $method], $param);
    }
}