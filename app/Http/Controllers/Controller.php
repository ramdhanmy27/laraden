<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Laraden\Support\Traits\LaradenController;

class Controller extends BaseController
{
    use LaradenController, AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
