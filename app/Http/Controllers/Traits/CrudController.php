<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Closure;
use App\Services\ParameterBag;
use Grid;

trait CrudController
{
	protected $crud;

    public function callAction($method, $param) {
    	$this->crudInit();
    	return parent::callAction($method, $param);
    }

	/**
	 * Initialize CRUD Controller
	 * 
	 * @return void
	 */
	public function crudInit(Closure $callback = null) {
		// CRUD Trait is already initialized
		if ($this->crud instanceof \StdClass)
			return;

		$id = config("controller.id");
		$this->crud = new \StdClass;

		// default parameter
		$this->crud->title = id2title($id);
		$this->crud->base_url = $id;
		$this->crud->table_field = null;

        if (is_callable($callback))
            $callback($this->crud);

        // create model instance
        if (!isset($this->crud->model))
            $this->crud->model = "App\\Models\\".id2camel($id);

        $this->crud->model_title = id2title(camel2id($this->crud->model));

        view()->share(["crud" => $this->crud]);
	}

	/**
     * List Data
     * 
     * @return View
     */
    public function getIndex() {
        $grid = Grid::model($this->crud->model, $this->crud->table_field);
		$grid->link("{$this->crud->base_url}/add", "Create ".$this->crud->model_title, "TR");
        $grid->add("action", "Action")->cell(function($val, $model) {
        	return Grid::action($model, $this->crud->base_url);
        });

    	return first_view([app("theme")->defaultPath(), "layout.crud.index"], compact('grid'));
    }

    /** Create */

    /**
     * Create Form Page
     * @return View
     */
    public function getAdd() {
    	return first_view([app("theme")->defaultPath(), "layout.crud.add"]);
    }

    /**
     * Create new data
     * 
     * @param  Request $req
     * @return Response
     */
    public function postAdd(Request $req) {
        $model = $this->crud->model;
        $model::create($req->except("_token"));
        
        return redirect($this->crud->base_url);
    }

    /** Update */

    /**
     * Update Form Page
     * 
     * @param  string $id
     * @return View
     */
    public function getEdit($id) {
    	$model = $this->crud->model;

    	return first_view([app("theme")->defaultPath(), "layout.crud.edit"], [
    		"model" => $model::findOrFail($id),
		]);
    }

    /**
     * Update Data
     * 
     * @param  Request $req
     * @param  string $id
     * @return Response
     */
    public function postEdit(Request $req, $id) {
        $model = $this->crud->model;
        $model::findOrFail($id)->update($req->except("_token"));

        return redirect($this->crud->base_url);
    }

    /**
     * Delete Data
     * 
     * @param  Request $req
     * @return Response
     */
    public function postDelete(Request $req, $id) {
        $model = $this->crud->model;
        $model::findOrFail($id)->delete();

    	return back();
    }

    /**
     * View Detail Data
     * 
     * @param  string $id
     * @return View
     */
    public function getView($id) {
    	$model = $this->crud->model;

    	return first_view([app("theme")->defaultPath(), "layout.crud.view"], [
    		"model" => $model::findOrFail($id),
    	]);
    }
}
