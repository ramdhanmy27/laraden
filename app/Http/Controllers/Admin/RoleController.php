<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CrudController;
use App\Http\Requests;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    use CrudController;

    public function __construct() {
    	$this->crudInit(function($config) {
    		$config->model = Role::class;
    		$config->form = [
    			["text", "name"],
    			["text", "display_name"],
    			["textarea", "description"],
    		];
    	});
    }
}
