<?php

namespace App\Providers;

use App\Services\Debug;
use App\Services\Messages;
use App\Services\Theme;
use App\Services\Widget\Grid;
use App\Services\Widget\Menu\Menu;
use App\Services\Widget\Slider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {}

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register Service Container
        $this->registerServices();

        // Init theme with namespace
        $this->loadTheme("resources/theme");

        // Init all applications
        $this->initApps(app_path("Apps"));
    }

    public function initApps($path) {
        $connection_tpl = config("database.connections.".config("database.default"));

        foreach (glob("$path/*") as $app_path) {
            $app_class = basename($app_path);
            $app_id = camel2id($app_class);

            /** Create app connection */
            $app_connection = $connection_tpl;
            $app_connection["schema"] = $app_id;

            // assign connections
            config(["database.connections.$app_id" => $app_connection]);
        }
    }

    public function loadTheme($path) {
        foreach (glob(base_path("$path/*", GLOB_ONLYDIR)) as $path) {
            // theme name
            $theme = basename($path);

            // add theme path to view finder
            \View::addNamespace($theme, "$path/views");
        }
    }

    public function registerServices() {
        $this->app->singleton("theme", function() {
            return new Theme;
        });

        $this->app->singleton("menu", function() {
            return new Menu;
        });

        $this->app->singleton("slider", function() {
            return new Slider;
        });
        
        $this->app->singleton("table.grid", function() {
            return new Grid;
        });
        
        $this->app->singleton("permission", function() {
            return new Permission;
        });
            
        $this->app->singleton("notif", function() {
            return new Messages("notif");
        });
        
        $this->app->singleton("flash", function() {
            return new Messages("flash");
        });
    }
}
