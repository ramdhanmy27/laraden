<?php

Route::group(["middleware" => ["web", "config:theme=backend|menu=router"]], function() {
	Route::menu(["title" => "Home", "url" => "admin", "icon" => "fa fa-home"]);

	Route::menu(["title" => "Settings", "icon" => "fa fa-cog"], function() {
		Route::controller("menu", "MenuController")
			->menu("Menu", "fa fa-bars");
			
		Route::controller("slider", "SliderController")
			->menu("Slider", "fa fa-object-group");
	});

	Route::controller("/", "HomeController");
});