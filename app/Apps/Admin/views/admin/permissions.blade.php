@extends("app")

@push("style")
<style>
    .fixed-button {
        position: fixed;
        top: 20%;
        right: 0;
    }
    .fixed-button .btn {
        border-radius: 5px 0px 0px 5px;
        box-shadow: -1px 1px 2px #999 !important;
    }
</style>
@endpush

@section("input", true)
@section("content")
<div class="panel panel-default">
	<div class="panel-heading"> Roles & Permissions </div>

	<div class="panel-body">
	{!! Form::open() !!}
		<table class="table table-responsive table-bordered table-striped table-hover">
			<thead>
				{{-- Roles --}}
				<tr class="dark">
					<th>Permission Name</th>

					@foreach ($roles as $id => $role)
						<th>{{ $role }}</th>
					@endforeach
				</tr>
			</thead>

			<tbody>
				<?php $colspan = count($roles) + 1; ?>

				{{-- Permissions --}}
				@foreach ($permissions as $group => $items)
					<tr class="primary"> 
						<th colspan="{{ $colspan }}">{{ ucwords(str_replace("-", " ", $group)) }}</th> 
					</tr>

					@foreach ($items as $perm)
						<tr>
							<td>
								<span>{{ $perm->display_name }}</span>
								<h6 class="text-muted">{{ $perm->description }}</h6>
							</td>

							@foreach ($roles as $id => $role)
								<td> {!! Form::toggleSwitch(
									"perm[$id][]", $perm->id, 
									in_array($id, explode(",", $perm->roles)), 
									["class" => "switch-sm switch-info"]) 
								!!} </td>
							@endforeach
						</tr>
					@endforeach
				@endforeach
			</tbody>
		</table>

		<div class='fixed-button'>
			<input class="btn btn-warning" type="submit" value="Simpan"></input>
	    </div>
	{!! Form::close() !!};
	</div>
</div>
@endsection