<?php

return [
	// to publish theme
	"theme" => env("MIX_THEME", "freelancer"),

	"module" => [
		// loaded module path
		"path" => [
			"app/Modules" => "App\\Modules\\",
			"vendor/ramdhanmy27/laraden-framework/modules" => "Laraden\\Modules\\",
		],
	],

	"menu" => [
		// [db, collection]
	    "default" => env("MENU_DRIVER", "collection"),

		"drivers" => [
		    // MENU_DRIVER should be "db"
			"db" => env("DB_CONNECTION", "pgsql"),
		],

		// separate menu per module
		"separate" => env("MENU_SEPARATE", true),
	],
];