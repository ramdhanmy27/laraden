@extends("backend::auth.app")

@section("title", "Sign Up")

@section("content")
<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
    {!! csrf_field() !!}

    <div class="form-group mb-lg {{ $errors->has('name') ? 'has-error' : '' }}">
        <label>Name</label>
        <input name="name" type="text" class="form-control input-lg" value="{{ old('name') }}" />

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group mb-lg {{ $errors->has('email') ? 'has-error' : '' }}">
        <label>E-mail Address</label>
        <input name="email" type="email" class="form-control input-lg" value="{{ old('email') }}">

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group mb-lg {{ $errors->has('password') ? 'has-error' : '' }}">
        <label>Password</label>
        <input name="password" type="password" class="form-control input-lg">

        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group mb-lg {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
        <label>Confirm Password</label>
        <input name="password_confirmation" type="password_confirmation" class="form-control input-lg">

        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
    </div>

    <div class="row">
        <div class="col-sm-8">
            <div class="checkbox-custom checkbox-default">
                <input id="AgreeTerms" name="agreeterms" type="checkbox"/>
                <label for="AgreeTerms">I agree with <a href="#">terms of use</a></label>
            </div>
        </div>
        <div class="col-sm-4 text-right">
            <button type="submit" class="btn btn-primary hidden-xs">Sign Up</button>
            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign Up</button>
        </div>
    </div>

    <span class="mt-lg mb-lg line-thru text-center text-uppercase">
        <span>or</span>
    </span>

    <p class="text-center">Already have an account? <a href="{{ url("login") }}">Sign In!</a>
</form>
@endsection