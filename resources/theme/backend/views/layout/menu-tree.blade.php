<?php

$class = [];

if (isset($menu->childView))
    $class[] = "nav-parent";

if ($menu->active === true) 
    $class[] = "nav-active";

?>

<li {!! count($class) ? "class='".implode(" ", $class)."'" : "" !!}>
    <a href='{{ isset($menu->url) ? url($menu->url) : "#" }}'>
        {!! !empty($menu->icon) ? "<i class='$menu->icon'></i>" : "" !!}
        <span>{{ $menu->title }}</span>
    </a>

    @if (isset($menu->childView))
	    <span class="toggler">
	    	<span class="glyphicon glyphicon-chevron-down"></span>
	    </span>
	    
        <ul class='nav nav-children'>{!! $menu->childView !!}</ul>
    @endif
</li>