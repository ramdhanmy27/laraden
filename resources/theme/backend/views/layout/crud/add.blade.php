@extends("backend::layout.panel")

@section("input", true)

@if (isset($crud))
	@section("title", isset($crud->model_title) ? "Create $crud->model_title" : $title)
@endif

@section("body")
	{!! first_view([app("theme")->defaultPath("form"), "layout.crud.form"], get_defined_vars())->render() !!}
@endsection