@extends("backend::app")

@section("title", "Silder")

@section("content")
	<div class="text-right">
		<button class="btn btn-default dz-clickable">
			<i class="fa fa-upload"></i> Upload
		</button>

		<a href="{{ url("admin/slider/delete-all") }}" method="POST" class="btn btn-danger" confirm="Are you sure ?">
			<i class="fa fa-trash"></i> Delete All
		</a>
	</div>

	<hr />

    <form method="post" enctype="multipart/form-data" class="dropzone" id="slider-photo">
        <div class="fallback">
            <input name="file" type="file" multiple="" />
        </div>
    </form>
@endsection

@push("style")
	<link rel="stylesheet" href="{{ asset("css/public/dropzone.css") }}" />
@endpush

@push("app-script")
	<script src="{{ asset("js/public/dropzone.min.js") }}"></script>
	<script src="{{ asset("js/app/admin/slider.js") }}"></script>
@endpush