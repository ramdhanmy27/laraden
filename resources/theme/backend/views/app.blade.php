<!DOCTYPE HTML>
<html class="fixed sidebar-left-collapsed">
    <head>
        <title>
            @if (trim($__env->yieldContent("title"))) 
                @yield('title')
            @else
                @section("title", "File Management") @show
            @endif
        </title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{ asset("backend::css/vendor.css") }}">
        @if ($__env->yieldContent("input"))
            <link rel="stylesheet" href="{{ asset("backend::css/input.css") }}">
        @endif

        @stack("style")
        <link rel="stylesheet" href="{{ asset("backend::css/app.css") }}">
        @stack("app-style")
    </head>
    <body>
        <section class="body">
            <!-- start: header -->
            <header class="header">
                <div class="logo-container">
                    <a href="../" class="logo">
                        <img src="{{ asset("backend::img/logo.png") }}" height="35" alt="Porto Admin" />
                    </a>
                    <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" 
                        data-target="html" data-fire-event="sidebar-left-opened">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
            
                <!-- start: search & user box -->
                <div class="header-right">
                    @if (Auth::check())
                        <div id="userbox" class="userbox">
                            <a href="#" data-toggle="dropdown">
                                <figure class="profile-picture">
                                    <img src="{{ asset("backend::img/user-default.jpg") }}" class="img-circle" 
                                        data-lock-picture="{{ asset("backend::img/user-default.jpg") }}" />
                                </figure>

                                <div class="profile-info">
                                    <span class="name">{{ Auth::user()->name }}</span>
                                    <span class="role">{{ Auth::user()->mail }}</span>
                                </div>
                
                                <i class="fa custom-caret"></i>
                            </a>
                
                            <div class="dropdown-menu">
                                <ul class="list-unstyled">
                                    <li>
                                        <a role="menuitem" tabindex="-1" href="{{ url('logout') }}">
                                            <i class="fa fa-power-off"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
                <!-- end: search & user box -->
            </header>
            <!-- end: header -->

            <div class="inner-wrapper">
                <!-- start: sidebar -->
                <aside id="sidebar-left" class="sidebar-left">
                    <div class="sidebar-header">
                        <div class="sidebar-title">
                            Navigation
                        </div>
                        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" 
                            data-target="html" data-fire-event="sidebar-left-toggle">
                            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                        </div>
                    </div>
                
                    <div class="nano">
                        <div class="nano-content">
                            <nav id="menu" class="nav-main" role="navigation">
                                {!! Menu::render() !!}
                            </nav>
                        </div>
                    </div>
                </aside>
                <!-- end: sidebar -->

                <section role="main" class="content-body">
                    <header class="page-header">
                        <h2>@yield("title")</h2>
                    
                        <div class="right-wrapper pull-right">
                            {!! Menu::renderBreadcrumb() !!}
                        </div>
                    </header>

                    <!-- Flash Messages -->
                    @if (Flash::exists())
                        @foreach (Flash::pull() as $state => $msg)
                            <div class='alert alert-{{ $state }}'>
                                <button type="button" class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>

                                {!! "<li>".implode("</li><li>", $msg)."</li>" !!}
                            </div>
                        @endforeach
                    @endif

                    {{-- Errors --}}
                    @if (isset($errors) && count($errors) > 0)
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">
                                <span>&times;</span>
                            </button>
                            
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @yield("content")
                </section>
            </div>
        </section>

        <!-- Modals -->
        @include("ui.modal", ["id" => "modal-basic"])
        @include("ui.modal", ["id" => "modal-error"])
        @include("ui.modal", [
            "id" => "modal-confirm", 
            "footer" => 
                "<button type='button' data-dismiss='modal' class='btn btn-primary modal-accept'>Ya</button>
                <button type='button' data-dismiss='modal' class='btn btn-default modal-close'>Tidak</button>"
        ])

        <!-- loading image -->
        <div class="hide">
            <img src="{{ asset("backend::img/loading.gif") }}" id="img-loading" />
        </div>

        <!-- Javascript -->
        <script src="{{ asset("backend::js/vendor.js") }}"></script>
        @if ($__env->yieldContent("input"))
            <script src="{{ asset("backend::js/input.js") }}"></script>
        @endif

        @stack("script")
        <script src="{{ asset("backend::js/app.js") }}"> </script>
        <script>
            fn.url.base = '{{ url("/") }}/';
            
            $(document).ready(function() {
                @if (Notif::exists())
                    var notification = {!! json_encode(Notif::pull()) !!};

                    for (var type in notification) 
                        fn.notif(notification[type], type);
                @endif
            })
        </script>
        @stack("app-script")
    </body>
</html>
