@extends('front::layout.layout')

@section('body')
    <section class="page-header">
        <div class="container">
            {!! Menu::renderBreadcrumb() !!}
            <h1>@yield("title")</h1>
        </div>
    </section>

    <div class="container">
        <div class="mt-xlg">
            <!-- Flash Messages -->
            @if (Flash::exists())
                @foreach (Flash::pull() as $state => $msg)
                    <div class='alert alert-{{ $state }}'>
                        <button type="button" class="close" data-dismiss="alert">
                            <span>&times;</span>
                        </button>

                        {!! "<li>".implode("</li><li>", $msg)."</li>" !!}
                    </div>
                @endforeach
            @endif

            {{-- Errors --}}
            @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @yield("content")
        </div>
    </div>
@endsection