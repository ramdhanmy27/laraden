@extends('front::app')

@section("title", "Login")

@section('content')
    <div class="featured-boxes">
		<div class="row">
			<div class="col-sm-6">
				<div class="featured-box featured-box-primary align-left mt-xlg">
					<div class="box-content">
						<h4 class="heading-primary text-uppercase mb-md">I'm a Returning Customer</h4>
						@yield("login")
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="featured-box featured-box-primary align-left mt-xlg">
					<div class="box-content">
						<h4 class="heading-primary text-uppercase mb-md">Register An Account</h4>
						@yield("register")
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection