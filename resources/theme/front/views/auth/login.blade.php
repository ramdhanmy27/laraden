@extends("front::auth.app")

@section("login")
<form action="{{ url("login") }}" method="post">
    {!! csrf_field() !!}

    <div class="form-group mb-lg {{ $errors->has('email') ? 'has-error' : '' }}">
        <label>Email</label>
        <div class="input-group input-group-icon">
            <input name="email" type="text" class="form-control input-lg" value="{{ old('email') }}" />
            <span class="input-group-addon">
                <span class="icon icon-lg">
                    <i class="fa fa-user"></i>
                </span>
            </span>
        </div>

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group mb-lg {{ $errors->has('password') ? 'has-error' : '' }}">
        <div class="clearfix">
            <label class="pull-left">Password</label>
            <a href="{{ url('/password/reset') }}" class="pull-right">Lost Password?</a>
        </div>
        <div class="input-group input-group-icon">
            <input name="password" type="password" class="form-control input-lg" />
            <span class="input-group-addon">
                <span class="icon icon-lg">
                    <i class="fa fa-lock"></i>
                </span>
            </span>
        </div>

        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="row">
        <div class="col-sm-8">
            <div class="checkbox-custom checkbox-default">
                <input id="RememberMe" name="remember" type="checkbox"/>
                <label for="RememberMe">Remember Me</label>
            </div>
        </div>
        <div class="col-sm-4 text-right">
            <button type="submit" class="btn btn-primary">Sign In</button>
        </div>
    </div>
</form>
@endsection

@section("register")
<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
    {!! csrf_field() !!}

    <div class="form-group mb-lg {{ $errors->has('name') ? 'has-error' : '' }}">
        <label>Name</label>
        <input name="name" type="text" class="form-control input-lg" value="{{ old('name') }}" />

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group mb-lg {{ $errors->has('email') ? 'has-error' : '' }}">
        <label>E-mail Address</label>
        <input name="email" type="email" class="form-control input-lg" value="{{ old('email') }}">

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="row">
        <div class="form-group mb-lg col-md-6 {{ $errors->has('password') ? 'has-error' : '' }}">
            <label>Password</label>
            <input name="password" type="password" class="form-control input-lg">

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group mb-lg col-md-6 {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
            <label>Confirm Password</label>
            <input name="password_confirmation" type="password_confirmation" class="form-control input-lg">

            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="row text-right">
        <button type="submit" class="btn btn-primary">Sign Up</button>
    </div>
</form>
@endsection