<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="bootstrap.min.css"></link>
		<style>
			* {font-family: sans-serif}
		</style>
		@stack("head")
	</head>

	<body> @yield("content") </body>
</html>>