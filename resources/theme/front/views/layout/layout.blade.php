<!DOCTYPE HTML>
<html>
<head>
    <title> @yield('title', "Front End") </title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset("front::img/favicon.ico") }}">

    <link rel="stylesheet" href="{{ asset("front::css/vendor.css") }}">
    @if ($__env->yieldContent("input"))
        <link rel="stylesheet" href="{{ asset("front::css/input.css") }}">
    @endif

    @stack("style")
    <link rel="stylesheet" href="{{ asset("front::css/app.css") }}">
    @stack("app-style")
</head>

<body>
    <!-- Header -->
    <header id="header" data-plugin-options='{
        "stickyEnabled": true, 
        "stickyEnableOnBoxed": true, 
        "stickyEnableOnMobile": true, 
        "stickyStartAt": 57, 
        "stickySetTop": "-57px", 
        "stickyChangeLogo": true}'>
        <div class="header-body">
            <div class="header-container container">
                <div class="header-row">
                    <div class="header-column">
                        <div class="header-logo">
                            <a href="{{ url("/") }}">
                                <img alt="Porto" width="160" height="44" data-sticky-width="140" 
                                    data-sticky-height="35" data-sticky-top="33" src="{{ asset("front::img/logo.png") }}">
                            </a>
                        </div>
                    </div>
                    <div class="header-column">
                        <div class="header-row">
                            <div class="header-search hidden-xs">
                                <form id="searchForm" method="get">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="q" id="q" 
                                            placeholder="Search..." required>

                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            <nav class="header-nav-top">
                                <ul class="nav nav-pills">
                                    <li class="hidden-xs"> <a href="login">Sign Up</a> </li>
                                    <li class="hidden-xs"> <a href="login">Login</a> </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="header-row">
                            {!! Menu::render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Body -->
    @yield("body")

    <!-- Footer -->
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="footer-ribbon">
                    <span>Get in Touch</span>
                </div>

                <div class="col-md-4">
                    <div class="contact-details">
                        <h4>Contact Us</h4>
                        <ul class="contact">
                            <p>
                                <i class="fa fa-map-marker"></i> 
                                <strong>Address:</strong> 1234 Street Name, City Name, United States
                            </p>
                            <p>
                                <i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-789
                            </p>
                            <p>
                                <i class="fa fa-envelope"></i> 
                                <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a>
                            </p>
                        </ul>
                    </div>
                </div>

                <div class="col-md-offset-6 col-md-2">
                    <h4>Follow Us</h4>
                    <ul class="social-icons">
                        <li class="social-icons-facebook">
                            <a href="http://www.facebook.com" target="_blank" title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="social-icons-twitter">
                            <a href="http://www.twitter.com" target="_blank" title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="social-icons-linkedin">
                            <a href="http://www.linkedin.com" target="_blank" title="Linkedin">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="footer-copyright">
            <div class="container">
                <p>© Copyright 2016. All Rights Reserved.</p>
            </div>
        </div>
    </footer>

    <!-- Modals -->
    @include("ui.modal", ["id" => "modal-basic"])
    @include("ui.modal", ["id" => "modal-error"])
    @include("ui.modal", [
        "id" => "modal-confirm", 
        "footer" => 
            "<button type='button' data-dismiss='modal' class='btn btn-primary modal-accept'>Ya</button>
            <button type='button' data-dismiss='modal' class='btn btn-default modal-close'>Tidak</button>"
    ])

    <!-- loading image -->
    <div class="hide">
        <img src="{{ asset('front::img/loading.gif') }}" id="img-loading" />
    </div>

    <!-- Javascript -->
    <script src="{{ asset("front::js/vendor.js") }}"></script>
    @if ($__env->yieldContent("input"))
        <script src="{{ asset("front::js/input.js") }}"></script>
    @endif

    @stack("script")
    <script src="{{ asset("front::js/app.js") }}"> </script>

    @stack("app-script")
    <script>
        $(document).ready(function() {
            @if (Notif::exists())
                var notification = {!! json_encode(Notif::pull()) !!};

                for (var type in notification) 
                    fn.notif(notification[type], type);
            @endif
        })
    </script>
</body>
</html>