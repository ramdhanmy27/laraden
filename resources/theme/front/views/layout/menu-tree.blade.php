<?php

$dropdown = isset($menu->childView);
$class = [];

if ($dropdown)
    $class[] = "dropdown";

if ($active === true) 
    $class[] = "active";

?>

<li {!! count($class) ? "class='".implode(" ", $class)."'" : "" !!}>
    <a {!! $dropdown ? 'class="dropdown-toggle"' : "" !!} href='{{ isset($menu->url) ? url($menu->url) : "#" }}'>
        {!! !empty($menu->icon) ? "<i class='$menu->icon'></i>" : "" !!}
        <span class="nowrap">{{ $menu->title }}</span>
    </a>

    @if ($dropdown)
        <ul class='dropdown-menu'>
            {!! $menu->childView !!}
        </ul>
    @endif
</li>