<?php

$translation = [];

foreach (rglob(__DIR__."/app/*.php") as $filepath) {
	$file = pathinfo($filepath);
	$translation[$file["filename"]] = include($filepath);
}

return $translation;