<?php

$data = [
	"month" => [
		"01" => "Januari",
		"02" => "Februari",
		"03" => "Maret",
		"04" => "April",
		"05" => "Mei",
		"06" => "Juni",
		"07" => "Juli",
		"08" => "Agustus",
		"09" => "September",
		"10" => "Oktober",
		"11" => "November",
		"12" => "Desember",
	],
];

// Year
$max = date("Y")+1;
$min = 1990;

for ($i = $max; $i > $min; $i--) {
	$data["year"][$i] = $i;
}

return $data;