<?php

return [
	"http" => [
		403 => "Anda tidak memiliki hak akses untuk halaman ini",
		404 => "Halaman tidak ditemukan",
	],
];