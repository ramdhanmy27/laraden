<div class="owl-carousel owl-theme" data-plugin-options='{
	"responsive": {
		"0": {"items": 1}, 
		"479": {"items": 2}, 
		"768": {"items": 3}, 
		"979": {"items": 4}, 
		"1199": {"items": 5}
	}, 
	"loop": false, 
	"autoHeight": true, 
	"margin": 10}'>
	@foreach ($list as $item)
		<div>
			<img class="img-responsive img-rounded" src="{{ asset($item) }}">
		</div>
	@endforeach
</div>