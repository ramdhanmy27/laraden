@extends("auth.app")

@section("title", "Recover Password")

@section("content")
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @else
        <div class="alert alert-info">
            <p class="m-none text-semibold h6">Enter your e-mail below and we will send you reset instructions!</p>
        </div>
    @endif

    <div class="featured-box featured-box-primary align-left mt-xlg">
        <div class="box-content">
            <h4 class="heading-primary text-uppercase mb-md">Enter your Email Account</h4>

            <form class="form-horizontal" role="form" method="POST" action="{{ url('password/email') }}">
                {!! csrf_field() !!}

                <div class="form-group mb-none">
                    <div class="input-group">
                        <input name="email" type="email" placeholder="E-mail" class="form-control input-lg" value="{{ old('email') }}" />

                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-lg" type="submit">Reset!</button>
                        </span>
                    </div>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <p class="text-center mt-lg">Remembered? <a href="{{ url("login") }}">Sign In!</a>
            </form>
        </div>
    </div>
@endsection 