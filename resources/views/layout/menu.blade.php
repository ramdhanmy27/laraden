<div class="header-nav">
    <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target="#menu-bar">
        <i class="fa fa-bars"></i>
    </button>

    <div id="menu-bar" class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
        <nav>
            <ul class="nav nav-pills">
			    {!! $menu !!}
            </ul>
        </nav>
    </div>
</div>