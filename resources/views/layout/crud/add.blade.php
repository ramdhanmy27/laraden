@extends("app")

@section("title", "Create $crud->model_title")
@section("input", true)

@section("content")
<div class="panel panel-featured panel-featured-primary">
	<div class="panel-heading">
		<h3 class="panel-title">@yield("title")</h3>
	</div>
	
	<div class="panel-body">
		{!! first_view([app("theme")->defaultPath("form"), "layout.crud.form"])->render() !!}
	</div>
</div>
@endsection