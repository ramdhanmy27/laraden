<?php

define('LARAVEL_START', microtime(true));

// ganti path ini dengan path vendor global jika perlu
// samakan dengan config vendor-dir pada composer.json
define("VENDOR_DIR", __DIR__."/../vendor");

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

require __DIR__.'/../app/Http/Helpers/debug.php';
require __DIR__.'/../app/Http/Helpers/helpers.php';

define("TENANT_DIR", dirname(__DIR__));
require VENDOR_DIR."/autoload.php";

/*
|--------------------------------------------------------------------------
| Include The Compiled Class File
|--------------------------------------------------------------------------
|
| To dramatically increase your application's performance, you may use a
| compiled class file which contains all of the classes commonly used
| by a request. The Artisan "optimize" is used to create this file.
|
*/

$compiledPath = __DIR__.'/cache/compiled.php';

if (file_exists($compiledPath)) {
    require $compiledPath;
}
