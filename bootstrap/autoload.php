<?php

define('LARAVEL_START', microtime(true));
define("VENDOR_DIR", __DIR__."/../vendor");

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so we do not have to manually load any of
| our application's PHP classes. It just feels great to relax.
|
*/


require VENDOR_DIR.'/ramdhanmy27/laraden-framework/helpers/override.php';
require __DIR__.'/../vendor/autoload.php';
